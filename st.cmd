epicsEnvSet("LLRF_P", "TS2-010RFC")
epicsEnvSet("LLRF_R", "RFS-LLRF-102")
epicsEnvSet("LLRF_DIG_R_1", "RFS-DIG-102")
epicsEnvSet("LLRF_RFM_R_1", "RFS-RFM-102")
epicsEnvSet("TSELPV" "TS2-010:Ctrl-EVR-101:EvtACnt-I.TIME")
epicsEnvSet("PVRFWDT", "TS2-010:Ctrl-EVM-101:RFWidth-RB")
epicsEnvSet("SEC", "ts2sec")
epicsEnvSet("RFSTATION", "4") # Cavity number

require essioc

epicsEnvSet("ENGINEER", "Gabriel Fedel <gabriel.fedel@ess.eu>")
## Add extra environment variables here
epicsEnvSet("TOP",      "$(E3_CMD_TOP)")

# Load standard module startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

# Load LLRF
require sis8300llrf, 5.29.4+0
require llrfsystem, 3.19.3+0

iocshLoad("$(llrfsystem_DIR)/llrfsystem.iocsh")

## For commands to be run after iocInit, use the function afterInit()

# Call iocInit to start the IOC
iocInit()
date
